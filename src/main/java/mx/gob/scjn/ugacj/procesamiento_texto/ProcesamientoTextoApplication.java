package mx.gob.scjn.ugacj.procesamiento_texto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ProcesamientoTextoApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		SpringApplication.run(ProcesamientoTextoApplication.class, args);
	}

}
