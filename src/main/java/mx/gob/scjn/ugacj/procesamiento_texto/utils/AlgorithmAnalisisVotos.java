package mx.gob.scjn.ugacj.procesamiento_texto.utils;

import java.text.Normalizer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.gob.scjn.ugacj.procesamiento_texto.dto.PayloadDTO;
import mx.gob.scjn.ugacj.procesamiento_texto.dto.SegmentoDTO;

public class AlgorithmAnalisisVotos {
	
	public String processNameMinistro(String ministro) {
		ministro = removeStopWords(ministro);
		ministro = stripAccents(ministro);
		return ministro.toLowerCase(Locale.ROOT);
	}
	
	public List<String> getFormatedMinistros(List<String> ministros){
		List<String> ministrosList = new ArrayList<String>();
		for(String ministro: ministros) {
			ministro = removeStopWords(ministro);
			ministro = stripAccents(ministro);
			ministrosList.add(ministro.toLowerCase());
		}
		return ministrosList;
	}
	
	public String removeStopWords(String text) {

		String[] allWords = text.split(" ");
		StringBuilder builder = new StringBuilder();
		for (String word : allWords) {
			if (!NlpUtils.STOPWORDS.contains(word)) {
				builder.append(word);
				builder.append(' ');
			}
		}
		return builder.toString().trim();
	}

	public String transformNumbers(String text) {
		for(String key : NlpUtils.NUMEROS.keySet()) {
			if (text.contains(key)) {
				text = text.replaceAll(key, " "+NlpUtils.NUMEROS.get(key)+" ");
			}
		}
		return text;
	}

	public String stripAccents(String s) {
		s = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return s;
	}
	
	public String removeExtraLines(String str) {
		str = str.replaceAll("(\r\n|\n)", " ");
		return str;
	}
	
	public static int textMatches(String text, String regexNumExpediente) {
		Pattern pattern = Pattern.compile(regexNumExpediente);
		Matcher matcher = pattern.matcher(text);
		if (matcher.find())
		{
			return matcher.start();
		}
		return -1;
	}
	
	public PayloadDTO findDecision(PayloadDTO payLoad, String regex, String decision) {
		try {
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(payLoad.getText());
			if (matcher.find())
			{
				String textFound = matcher.group().toString();
				pattern = Pattern.compile(NlpUtils.REGEX_NUMBERS);
				matcher = pattern.matcher(textFound);
				if (matcher.find())
				{
					String number = matcher.group().toString().trim();
					payLoad.setNumVotacionDecision(Integer.parseInt(number));
					payLoad.setDecision(decision);
					return payLoad;
				}
			}
		}catch(Exception e) {
			System.out.println(e);
		}
		return payLoad;
	}
	
	public PayloadDTO getSentido(PayloadDTO payLoad) {
		for(String regex : NlpUtils.SENTIDO_LIST) {
			try {
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(payLoad.getText());
				if (matcher.find())
				{
					payLoad.setSentidoMayoria(regex.toUpperCase());
					return payLoad;
				}
			}catch(Exception e) {
				System.out.println(e);
			}
		}
		return payLoad;
	}
	
	public PayloadDTO getDecision(PayloadDTO payLoad) {
		payLoad = getSentido(payLoad);
		for(String regex : NlpUtils.RECHAZADO_LIST) {
			payLoad = findDecision(payLoad, regex, NlpUtils.RECHAZADO);
			if(payLoad.getDecision()!=null) {
				if(payLoad.getDecision().equals(NlpUtils.RECHAZADO)) {
					return payLoad;
				}
			}
		}
		
		for(String regex : NlpUtils.APROBADO_LIST) {
			payLoad = findDecision(payLoad, regex, NlpUtils.APROBADO);
			if(payLoad.getDecision()!=null) {
				if(payLoad.getDecision().equals(NlpUtils.APROBADO)) {
					return payLoad;
				}
			}
		}
		return payLoad;
		
	}
	
	//Segmentos******
	public Map<Integer, String> agregarCoincidencias(String text, List<String> keyWordsList, Map<Integer, String> coincidencias) {
		for(String regex:keyWordsList){
			int pos = textMatches(text, regex);
			if(pos>=0) {
				if(coincidencias.get(pos)!=null) {
					if(regex.length()>coincidencias.get(pos).length()) {
						coincidencias.put(pos,regex);
					}
				}else {
					coincidencias.put(pos,regex);
				}
			}
		}
		return coincidencias;
	}
	
	public SegmentoDTO getSegmentoFromClosestCoincidencia(String text, Map<Integer, String> coincidencias, SegmentoDTO segmentoDto) {
		//Obtenemos la coincidencia más cercana
		String regex = coincidencias.entrySet().stream().min((entry1, entry2) -> entry1.getKey() > entry2.getKey()  ? 1 : -1).get().getValue();
		//Devolvemos la parte del texto solicitado
		String[] txtSegment = text.split(regex, 2);
		segmentoDto.setTexto(text);
		segmentoDto.setCoincidencia(regex);
		segmentoDto.setSegmentoIzquierdo(txtSegment[0].trim());
		segmentoDto.setSegmentoDerecho(txtSegment[1].trim());
		return segmentoDto;
	}

	public SegmentoDTO getSegmentServible(String text, List<String> keyWordsList) {
		Map<Integer, String> coincidencias =  new HashMap<>();
		List<SegmentoDTO> segmentoList = new ArrayList<SegmentoDTO>();
		SegmentoDTO segmentoDto = new SegmentoDTO();
		
		coincidencias = agregarCoincidencias(text, keyWordsList, coincidencias);
		
		if(coincidencias.size()>0) {
			segmentoDto = getSegmentoFromClosestCoincidencia(text, coincidencias, segmentoDto);
		}
		return segmentoDto;
	}
	
	public SegmentoDTO getSegment(String text, List<String> keyWordsList, List<String> keyWordsList2, List<String> keyWordsList3,
			String sentidoResolucion) {
		Map<Integer, String> coincidencias =  new HashMap<>();
		SegmentoDTO segmentoDto = new SegmentoDTO();
		if(text == null || text.equalsIgnoreCase("")){
			return null;
		}
		coincidencias = agregarCoincidencias(text, keyWordsList, coincidencias);
		coincidencias = agregarCoincidencias(text, keyWordsList2, coincidencias);
		coincidencias = agregarCoincidencias(text, keyWordsList3, coincidencias);
		
		if(coincidencias.size()>0) {
			segmentoDto = getSegmentoFromClosestCoincidencia(text, coincidencias, segmentoDto);
//			System.out.println("Ministro: "+segmentoDto.getSegmentoIzquierdo()+", Voto: "+segmentoDto.getCoincidencia());
			if(keyWordsList.contains(segmentoDto.getCoincidencia())) {
				segmentoDto.setDecision(NlpUtils.APROBADO);
			}else if(keyWordsList2.contains(segmentoDto.getCoincidencia())) {
				segmentoDto.setDecision(NlpUtils.RECHAZADO);
			}else if(keyWordsList3.contains(segmentoDto.getCoincidencia())) {
				segmentoDto.setDecision(sentidoResolucion);
			}
			return segmentoDto;
		}
		return null;
	}
	
	public List<SegmentoDTO> getIntents(SegmentoDTO segmentoServible, String sentidoDecision) {
		List<SegmentoDTO> segmentosList = new ArrayList<SegmentoDTO>();
		boolean stop = false;
		String segmentoDividirTxt =  segmentoServible.getSegmentoDerecho();
		while(!stop) {
			SegmentoDTO segmento = getSegment(segmentoDividirTxt, NlpUtils.VOTO_FAVOR, NlpUtils.VOTO_CONTRA,  
					NlpUtils.VOTO_SENTIDO_RESOLUCION, sentidoDecision);
			if(segmento!=null) {
				if(segmento.getSegmentoDerecho()!=null||!segmento.getSegmentoDerecho().equals("")) {
					segmentoDividirTxt = segmento.getSegmentoDerecho();
					segmentosList.add(segmento);
				}else {
					stop = true;
					segmento.setSegmentoDerecho(null);
				}
			}else {
				stop = true;
			}
		}
		return segmentosList;
	}
	//
	
	public List<String> findMinistrosInText(List<String> ministrosList, String segmentTxt){
		List<String> ministrosFound = new ArrayList<String>();	
		for(String ministro: ministrosList) {
			if(segmentTxt.contains(ministro)) {
				ministrosFound.add(ministro);
			}
		}
		return ministrosFound;
	}
	
	public List<String> parseNameMinistros(List<String> listMinistros, List<String> catalogoMinistros) {
		List<String> ministrosCatFormattedList = getFormatedMinistros(catalogoMinistros);
		for(int i=0; i<listMinistros.size(); i++) {
			for(int j=0; i<ministrosCatFormattedList.size(); j++) {
				if(listMinistros.get(i).equals(ministrosCatFormattedList.get(j))) {
					listMinistros.set(i,catalogoMinistros.get(j));
					break;
				}
			}
		}
		return listMinistros;
	}
	
	public PayloadDTO getSentidoAndMinistrosVotacionByText(PayloadDTO payLoad, String text) {
		List<String> ministrosList = getFormatedMinistros(NlpUtils.LIST_MINISTROS_FULLNAME);
		if(text== null || text.equalsIgnoreCase("")){
			return null;
		}
		List<String> ministrosFound = findMinistrosInText(ministrosList, text);
		ministrosFound = parseNameMinistros(ministrosFound, NlpUtils.LIST_MINISTROS_FULLNAME);
		payLoad.getMinistrosAFavor().addAll(ministrosFound);
		payLoad.setNumVotacionDecision(ministrosFound.size());
		payLoad = getSentido(payLoad);
		return payLoad;
	}
	
	public PayloadDTO getVotacionMinistros(List<SegmentoDTO> segmentosList, PayloadDTO payLoad, String text) {
		List<String> ministrosList = getFormatedMinistros(NlpUtils.LIST_MINISTROS);
		if(text== null || text.equalsIgnoreCase("")){
			return null;
		}
		List<String> ministrosFound = findMinistrosInText(ministrosList, text);
		if(payLoad.getDecision().equals(NlpUtils.APROBADO)||payLoad.getDecision().equals(null)) {
			payLoad.getMinistrosAFavor().addAll(ministrosFound);
		}else if(payLoad.getDecision().equals(NlpUtils.RECHAZADO)){
			payLoad.getMinistrosEnContra().addAll(ministrosFound);
		}
		return payLoad;
	}
	
	public PayloadDTO getVotacionMinistros(List<SegmentoDTO> segmentosList, PayloadDTO payLoad) {
		//Obtenemos la lista de todos los ministros existentes, con la que compararemos
		List<String> ministrosList = getFormatedMinistros(NlpUtils.LIST_MINISTROS);
		
		for(SegmentoDTO segmento : segmentosList) {
			//Buscamos a los ministros en el texto
			List<String> ministrosFound = findMinistrosInText(ministrosList, segmento.getSegmentoIzquierdo());
			if(segmento.getDecision().equals(NlpUtils.APROBADO)||segmento.getDecision().equals(null)) {
				payLoad.getMinistrosAFavor().addAll(ministrosFound);
			}else if(segmento.getDecision().equals(NlpUtils.RECHAZADO)){
				payLoad.getMinistrosEnContra().addAll(ministrosFound);
			}	
		}
		return payLoad;
	}
	
	public PayloadDTO getMinistrosVotacion(PayloadDTO payLoad) {
		//Paso 1. Dividimos el texto en 2 partes. la primera sección inservible y la segunda el segmento útil
		SegmentoDTO segmentoServible = getSegmentServible(payLoad.getText(), NlpUtils.INIT_MINISTROS);
		//Paso 2. Segmentamos texto por intensión
		List<SegmentoDTO> segmentosList = getIntents(segmentoServible, payLoad.getDecision());

		if(segmentosList.size()>0) {
			//Paso 3. Identificamos contradicción en la primera división
			if(!segmentosList.get(0).getDecision().equals(payLoad.getDecision())) {
				SegmentoDTO segmentoContradictorio = segmentosList.get(0);
				SegmentoDTO segmentoFaltante = getSegmentServible(segmentoContradictorio.getTexto(), NlpUtils.INIT_MINISTROS);
				
				//Reescribo el objeto inicial para eliminar la contradicción
				segmentoFaltante.setCoincidencia(null);
				segmentoFaltante.setDecision(payLoad.getDecision());
				segmentoFaltante.setSegmentoIzquierdo(segmentoContradictorio.getSegmentoIzquierdo());
				
				//Edito y agrego el objeto nuevo, que no se identificó en la primera división
				segmentoContradictorio.setSegmentoIzquierdo(segmentoContradictorio.getSegmentoDerecho());
				segmentosList.add(0, segmentoContradictorio);
			}
			return getVotacionMinistros(segmentosList, payLoad);
		}else {
			return getVotacionMinistros(segmentosList, payLoad, segmentoServible.getSegmentoDerecho());
		}
	}
	
	
}
