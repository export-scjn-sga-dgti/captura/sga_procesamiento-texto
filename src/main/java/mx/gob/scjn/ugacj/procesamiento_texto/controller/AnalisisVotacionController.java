package mx.gob.scjn.ugacj.procesamiento_texto.controller;


import mx.gob.scjn.ugacj.procesamiento_texto.dto.PayloadDTO;
import mx.gob.scjn.ugacj.procesamiento_texto.service.AnalisisVotacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/analisis-votacion/")
public class AnalisisVotacionController {

    @Autowired
    private AnalisisVotacionService analisisVotServ;

    @CrossOrigin()
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    PayloadDTO getAnalisisVotacion(@RequestBody PayloadDTO payload) {
        return analisisVotServ.analisisTextoVotacion(payload);
    }

}