package mx.gob.scjn.ugacj.procesamiento_texto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.gob.scjn.ugacj.procesamiento_texto.dto.PayloadDTO;
import mx.gob.scjn.ugacj.procesamiento_texto.service.AnalisisNotaPrecedenteService;

@RestController
@RequestMapping(value = "/v1/analisis/notap/")
public class AnalisisNotaPrecedente {
	
	 @Autowired
	    private AnalisisNotaPrecedenteService analisisNotaP;

	    @CrossOrigin()
	    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody
	    PayloadDTO getAnalisisVotacion(@RequestBody PayloadDTO payload) {
	        return null;
	    }

}
