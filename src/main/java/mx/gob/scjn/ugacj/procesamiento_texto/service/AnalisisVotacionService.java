package mx.gob.scjn.ugacj.procesamiento_texto.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import mx.gob.scjn.ugacj.procesamiento_texto.dto.MinistrosDTO;
import mx.gob.scjn.ugacj.procesamiento_texto.dto.PayloadDTO;
import mx.gob.scjn.ugacj.procesamiento_texto.utils.AlgorithmAnalisisVotos;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

@Service
public class AnalisisVotacionService {

    public PayloadDTO analisisTextoVotacion(PayloadDTO payLoad) {

        AlgorithmAnalisisVotos algAnalisisV = new AlgorithmAnalisisVotos();
        String textOriginal = payLoad.getText();
        String text = payLoad.getText();
        if (text.equalsIgnoreCase("") || text == null) {
            return null;
        }
        text = algAnalisisV.removeStopWords(text);
        text = algAnalisisV.stripAccents(text);
        text = algAnalisisV.removeExtraLines(text);
        text = text.toLowerCase();
        text = algAnalisisV.transformNumbers(text);

        payLoad.setText(text);

        if (payLoad.getDecision() != null) {
            payLoad = algAnalisisV.getSentidoAndMinistrosVotacionByText(payLoad, text);
            payLoad.setText(textOriginal);
            return payLoad;
        }
        payLoad = algAnalisisV.getSentido(payLoad);
        payLoad = algAnalisisV.getDecision(payLoad);
        payLoad = algAnalisisV.getMinistrosVotacion(payLoad);
        if (payLoad != null) {
            payLoad.setText(textOriginal);
        }

        if (payLoad != null) {
            //recorrer ministros con catálogo para asignar el nombre completo
            List<MinistrosDTO> ministrosCompleto = readJsonMinistros();
            payLoad.setMinistrosAFavor(getMinistroNombreCompleto(payLoad.getMinistrosAFavor(), ministrosCompleto));
            payLoad.setMinistrosEnContra(getMinistroNombreCompleto(payLoad.getMinistrosEnContra(), ministrosCompleto));
        }

        return payLoad;
    }

    private Set<String> getMinistroNombreCompleto(Set<String> listMinistros, List<MinistrosDTO> ministrosCompleto) {
        Set<String> listMinistrosNombreCompleto = new HashSet<>();
        if (listMinistros != null && listMinistros.size() > 0) {
            listMinistros.forEach(m -> {
                listMinistrosNombreCompleto.add(ministrosCompleto.stream().filter(xx -> xx.getApellidos().equals(m)).map(MinistrosDTO::getNombre).findAny().get());
            });
        }
        return listMinistrosNombreCompleto;
    }

    private List<MinistrosDTO> readJsonMinistros() {
        AlgorithmAnalisisVotos algAnalisisV = new AlgorithmAnalisisVotos();
        List<MinistrosDTO> ministros = new ArrayList<>();
        try {
            File resource = new ClassPathResource("/static/JsonMinistros.json").getFile();
            String text = new String(Files.readAllBytes(resource.toPath()));
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            JavaType type = mapper.getTypeFactory().constructParametrizedType(List.class, List.class, MinistrosDTO.class);
            ministros = mapper.readValue(text, type);

            for (MinistrosDTO m : ministros) {
                m.setApellidos(algAnalisisV.processNameMinistro(m.getApellidos()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ministros;
    }

}