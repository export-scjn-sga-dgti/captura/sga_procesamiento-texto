package mx.gob.scjn.ugacj.procesamiento_texto.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NlpUtils {
	
	public static final String APROBADO = "APROBADO";
	public static final String RECHAZADO = "RECHAZADO";
	

	public static final List<String> STOPWORDS = Arrays.asList("la", "de", "de", "que", "el", "en", "y", "a", "los",
			"del", "se", "las", "por", "un", "para", "con", "no", "una", "su", "al", "lo", "como", "más", "pero", "sus",
			"le", "ya", "o", "este", "sí", "porque", "esta", "entre", "cuando", "muy", "sin", "sobre", "también", "me",
			"hasta", "hay", "donde", "quien", "desde", "todo", "nos", "durante", "todos", "uno", "les", "ni",
			"otros", "ese", "eso", "ante", "ellos", "e", "esto", "mí", "antes", "algunos", "qué", "unos", "yo", "otro",
			"otras", "otra", "él", "tanto", "esa", "estos", "mucho", "quienes", "nada", "muchos", "cual", "poco",
			"ella", "estar", "estas", "algunas", "algo", "nosotros", "mi", "mis", "tú", "te", "ti", "tu", "tus",
			"ellas", "nosotras", "vosotros", "vosotras", "os", "mío", "mía", "míos", "mías", "tuyo", "tuya", "tuyos",
			"tuyas", "suyo", "suya", "suyos", "suyas", "nuestro", "nuestra", "nuestros", "nuestras", "vuestro",
			"vuestra", "vuestros", "vuestras", "esos", "esas", "estoy", "estás", "está", "estamos", "estáis", "están",
			"esté", "estés", "estemos", "estéis", "estén", "estaré", "estarás", "estará", "estaremos", "estaréis",
			"estarán", "estaría", "estarías", "estaríamos", "estaríais", "estarían", "estaba", "estabas", "estábamos",
			"estabais", "estaban", "estuve", "estuviste", "estuvo", "estuvimos", "estuvisteis", "estuvieron",
			"estuviera", "estuvieras", "estuviéramos", "estuvierais", "estuvieran", "estuviese", "estuvieses",
			"estuviésemos", "estuvieseis", "estuviesen", "estando", "estada", "estadas", "estad", "he", "has", "ha",
			"hemos", "habéis", "han", "haya", "hayas", "hayamos", "hayáis", "hayan", "habré", "habrás", "habrá",
			"habremos", "habréis", "habrán", "habría", "habrías", "habríamos", "habríais", "habrían", "había", "habías",
			"habíamos", "habíais", "habían", "hube", "hubiste", "hubo", "hubimos", "hubisteis", "hubieron", "hubiera",
			"hubieras", "hubiéramos", "hubierais", "hubieran", "hubiese", "hubieses", "hubiésemos", "hubieseis",
			"hubiesen", "habiendo", "habido", "habida", "habidos", "habidas", "soy", "eres", "es", "somos", "sois",
			"son", "sea", "seas", "seamos", "seáis", "sean", "seré", "serás", "será", "seremos", "seréis", "serán",
			"sería", "serías", "seríamos", "seríais", "serían", "era", "eras", "éramos", "erais", "eran", "fui",
			"fuiste", "fue", "fuimos", "fuisteis", "fueron", "fuera", "fueras", "fuéramos", "fuerais", "fueran",
			"fuese", "fueses", "fuésemos", "fueseis", "fuesen", "siendo", "sido", "tengo", "tienes", "tiene", "tenemos",
			"tenéis", "tienen", "tenga", "tengas", "tengamos", "tengáis", "tengan", "tendré", "tendrás", "tendrá",
			"tendremos", "tendréis", "tendrán", "tendría", "tendrías", "tendríamos", "tendríais", "tendrían", "tenía",
			"tenías", "teníamos", "teníais", "tenían", "tuve", "tuviste", "tuvo", "tuvimos", "tuvisteis", "tuvieron",
			"tuviera", "tuvieras", "tuviéramos", "tuvierais", "tuvieran", "tuviese", "tuvieses", "tuviésemos",
			"tuvieseis", "tuviesen", "teniendo", "tenido", "tenida", "tenidos", "tenidas", "tened", "de", "en", "a",
			"del", "para", "por", "presidente", "*");
	
	public static final List<String> SENTIDO_LIST = Arrays.asList(
			"mayoria", 
			"unanimidad");
	
	public static final List<String> APROBADO_LIST = Arrays.asList(
			"aprobo mayoria [0-9]{1,2} votos", 
			"aprobo unanimidad [0-9]{1,2} votos",
			"mayoria [0-9]{1,2} votos",
			"aprobo [0-9]{1,2} votos");
	
	public static final List<String> RECHAZADO_LIST = Arrays.asList("mayoria [0-9]{1,2} votos contra");
	
	public static final List<String> INIT_MINISTROS = Arrays.asList("senoras ministras senores ministros",
			"senora ministra senores ministros", "senor ministro senoras ministras", "senores ministros", "senoras ministras");
	
	public static final List<String> LIST_MINISTROS = Arrays.asList(
			"Gutiérrez Ortiz Mena",
			"González Alcántara Carrancá",
			"Esquivel Mossa", 
			"Franco González Salas", 
			"Aguilar Morales",
			"Pardo Rebolledo",
			"Piña Hernández",
			"Ríos Farjat",
			"Laynez Potisek",
			"Zaldívar Lelo de Larrea",
			"Pérez Dayán",			
			"Medina Mora I.",
			"Cossío Díaz",
			"Luna Ramos",
			"Silva Meza",
			"Sánchez Cordero",
			"Valls Henández",
			"Ortiz Mayagoitia",
			"Aguirre Anguiano",
			"Gudiño Pelayo",
			"Góngora Pimentel",
			"Azuela Güitrón",
			"Díaz Romero",
			"Román Palacios",
			"Aguinaco Alemán",
			"Castro y Castro",
			"Ortiz Ahlf"
			);
	
	public static final List<String> LIST_MINISTROS_FULLNAME = Arrays.asList(
			"JOSÉ VICENTE AGUINACO ALEMÁN",
			"HUMBERTO ROMÁN PALACIOS",
			"GUILLERMO I. ORTIZ MAYAGOITIA",
			"SALVADOR AGUIRRE ANGUIANO",
			"GENARO D. GÓNGORA PIMENTEL",
			"JOSÉ DE JESÚS GUDIÑO PELAYO",
			"ARTURO ZALDÍVAR LELO DE LARREA",
			 "JAVIER LAYNEZ POTISEK",
			 "ANA MARGARITA RÍOS FARJAT",
			 "NORMA LUCÍA PIÑA HERNÁNDEZ",
			 "ALBERTO PÉREZ DAYÁN",
			 "EDUARDO MEDINA MORA I.",
			 "JORGE MARIO PARDO REBOLLEDO",
			 "LUIS MARÍA AGUILAR MORALES",
			 "JOSÉ FERNANDO FRANCO GONZÁLEZ SALAS",
			 "YASMÍN ESQUIVEL MOSSA",
			 "JUAN LUIS GONZÁLEZ ALCÁNTARA CARRANCÁ",
			 "ALFREDO GUTIÉRREZ ORTIZ MENA",
			 "JUAN DÍAZ ROMERO",
			 "MARIANO AZUELA GÜITRÓN",
			 "JOSÉ RAMÓN COSSÍO DÍAZ",
			 "MARGARITA BEATRIZ LUNA RAMOS",
			 "JUAN N. SILVA MEZA",
			 "OLGA MARÍA DEL CARMEN SÁNCHEZ CORDERO",
			 "SERGIO A. VALLS HERNÁNDEZ",
			 "JUVENTINO V. CASTRO Y CASTRO",
			 "LORETTA ORTIZ AHLF"			 
			);
	
	public static final List<String> VOTO_FAVOR = Arrays.asList("voto concurrente", "votos concurrentes", 
			"reservo derecho formular voto concurrente", "votaron favor", "voto aclaratorio");
	
	public static final List<String> VOTO_SENTIDO_RESOLUCION = Arrays.asList("apartandose consideraciones", "reserva criterio",
			"contra consideraciones", "contra metodologia", "unicamente tema competencial", "separandose consideraciones",
			"respecto considerando", "respecto considerandos", "respecto apartado", "respecto apartados");
	
	public static final List<String> VOTO_CONTRA = Arrays.asList("votaron contra", "voto contra", "voto particular", "votos particulares", 
			"votaron unicamente validez porcion normativa", "voto invalidez totalidad articulo");
	
	public static  Map<String, Integer> NUMEROS =  new HashMap<>();
	static {
		NUMEROS.put(" uno ", 1);
		NUMEROS.put(" dos ", 2);
		NUMEROS.put(" tres ", 3);
		NUMEROS.put(" cuatro ", 4);
		NUMEROS.put(" cinco ", 5);
		NUMEROS.put(" seis ", 6);
		NUMEROS.put(" siete ", 7);
		NUMEROS.put(" ocho ", 8);
		NUMEROS.put(" nueve ", 9);
		NUMEROS.put(" diez ", 10);
		NUMEROS.put(" once ", 11);
	}
	
	public static final String REGEX_NUMBERS = "[0-9]{1,2}";
}
