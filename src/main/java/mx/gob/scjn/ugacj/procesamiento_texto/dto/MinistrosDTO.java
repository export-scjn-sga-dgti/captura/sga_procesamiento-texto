package mx.gob.scjn.ugacj.procesamiento_texto.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.stream.Stream;

//@JsonInclude(JsonInclude.Include.NON_NULL)
public class MinistrosDTO  {

    private String _id;
    private String nombre;
    private  String apellidos;
    private String catalogo;
    private  String primeraSesion;
    private String ultimaSesion;
    private String fechaInicio;
    private  String fechaFin;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(String catalogo) {
        this.catalogo = catalogo;
    }

    public String getPrimeraSesion() {
        return primeraSesion;
    }

    public void setPrimeraSesion(String primeraSesion) {
        this.primeraSesion = primeraSesion;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Override
    public String toString() {
        return "MinistrosDTO{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", catalogo='" + catalogo + '\'' +
                ", primeraSesion='" + primeraSesion + '\'' +
                ", ultimaSesion='" + ultimaSesion + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                '}';
    }


}
