package mx.gob.scjn.ugacj.procesamiento_texto.dto;

import java.util.LinkedHashSet;
import java.util.Set;

public class PayloadDTO {
	
	String text;
	String decision;
	String sentidoMayoria;
	int numVotacionDecision;
	Set<String> ministrosAFavor = new LinkedHashSet<>();
	Set<String> ministrosEnContra = new LinkedHashSet<>();
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public int getNumVotacionDecision() {
		return numVotacionDecision;
	}

	public void setNumVotacionDecision(int numVotacionDecision) {
		this.numVotacionDecision = numVotacionDecision;
	}

	public Set<String> getMinistrosAFavor() {
		return ministrosAFavor;
	}

	public void setMinistrosAFavor(Set<String> ministrosAFavor) {
		this.ministrosAFavor = ministrosAFavor;
	}

	public Set<String> getMinistrosEnContra() {
		return ministrosEnContra;
	}

	public void setMinistrosEnContra(Set<String> ministrosEnContra) {
		this.ministrosEnContra = ministrosEnContra;
	}

	public String getSentidoMayoria() {
		return sentidoMayoria;
	}

	public void setSentidoMayoria(String sentidoMayoria) {
		this.sentidoMayoria = sentidoMayoria;
	}

	
	
	
	
	

}
