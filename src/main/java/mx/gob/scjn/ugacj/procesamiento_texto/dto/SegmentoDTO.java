package mx.gob.scjn.ugacj.procesamiento_texto.dto;

public class SegmentoDTO {
	
	public String texto;
	public String coincidencia;
	public String segmentoIzquierdo;
	public String segmentoDerecho;
	public String decision;
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getCoincidencia() {
		return coincidencia;
	}
	public void setCoincidencia(String coincidencia) {
		this.coincidencia = coincidencia;
	}
	public String getSegmentoIzquierdo() {
		return segmentoIzquierdo;
	}
	public void setSegmentoIzquierdo(String segmentoIzquierdo) {
		this.segmentoIzquierdo = segmentoIzquierdo;
	}
	public String getSegmentoDerecho() {
		return segmentoDerecho;
	}
	public void setSegmentoDerecho(String segmentoDerecho) {
		this.segmentoDerecho = segmentoDerecho;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}

}
